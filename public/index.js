(function(){

    var MyApp = angular.module("MyApp", []);

    var MyCtrl = function($http){
        var myCtrl = this;
        myCtrl.email = "";     
        myCtrl.pass = "";
        
        myCtrl.name = "";
        myCtrl.gender = "";
        myCtrl.DOB = "";
        myCtrl.address = "";
        myCtrl.country = "";
        myCtrl.contactnum = "";

        

        myCtrl.inputsubmit = function(){
        console.log("submit clicked")
        myCtrl.passwordcheck();
        myCtrl.agecheck();
        $http.get("/register", {
                params: {
                    name: myCtrl.name,
                    email: myCtrl.email,
                    pass: myCtrl.pass,
                    gender: myCtrl.gender,
                    DOB: myCtrl.DOB,
                    address: myCtrl.address,
                    country: myCtrl.country,
                    contactnum: myCtrl.contactnum,
                }
            }).then(function(result) {
                myCtrl.message = result.data.reply1;
            }).catch(function() {
                myCtrl.message = result.data.reply2;
            })
        myCtrl.inputreset();
        };


    myCtrl.passwordcheck = function() {
            var passcondition = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,100}$/;
            if (myCtrl.pass.match(passcondition)) {
                return "true";
            } else {
            return "false";

            }
    }; 


    myCtrl.agecheck = function () {
        var minage = 1999;
        var convage = myCtrl.DOB.getUTCFullYear() 
        var actual = (minage - convage);
        console.log(myCtrl.DOB.getUTCFullYear());
        if (actual < 0) {
            alert('User has to be older than 18');
        }
            else {
                ;
            }
        }

    myCtrl.inputreset= function() {    
            myCtrl.name="";
            myCtrl.email="";
            myCtrl.pass ="";
            myCtrl.gender="";
            myCtrl.address="";
            myCtrl.DOB="";
            myCtrl.country="";
            myCtrl.number="";
        };
 };




    MyCtrl.$inject = ["$http"];
    MyApp.controller ("MyCtrl", MyCtrl)






})();